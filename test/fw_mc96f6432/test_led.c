/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <8051.h>
#include <compiler.h>

/* this is just a dirty test hack that blinks an LED on Port 4 bit 0 */

SFR(P4,   0xA0);
SFR(P4IO, 0xC9);
SFR(P4PU, 0xA3);
SFR(P4OD, 0x94);

SFR(OSCCR, 0xc8);


void DELAY (unsigned int delay){
	unsigned char x;
	unsigned int j;
	for(j=0; j<delay; j++) {
		for(x=0; x<50; x++) {}
	}
}


void main() {

    // default internal clock is 1MHZ.
    // change prescaler to 8MHZ
    OSCCR=0x20;
    
    // DDR: out
    P4IO |= 0x01;
    
    while (1) {
        P4 |= 0x01;
        DELAY(500);
        P4 &= ~0x01;
        DELAY(500);
    }
    return;
}


