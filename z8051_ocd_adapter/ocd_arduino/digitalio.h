/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

/*
 *  Arduino Pin Mapping
 * D2       PD2
 * D3       PD3
 * D4       PD4
 * D5       PD5
 * D6       PD6
 * D7       PD7
 * D8       PB0
 * D9       PB1
 * D10      PB2
 * D11      PB3
 * D12      PB4
 * D13      PB5
 * Aref
 * ADC0     PC0
 * ADC1     PC1
 * ADC2     PC2
 * ADC3     PC3
 * ADC4     PC4
 * ADC5     PC5
 * ADC6     ??
 * ADC7     ??
 */

#define INPUT       0
#define OUTPUT      1

#define LOW         0
#define HIGH        1


static inline void pinMode(volatile  uint8_t *ddr, uint8_t bit, uint8_t direction) __attribute__((always_inline));
static inline void digitalWrite(volatile uint8_t *port, uint8_t bit, uint8_t state) __attribute__((always_inline));
static inline uint8_t digitalRead(volatile uint8_t *pin, uint8_t bit) __attribute__((always_inline));

static inline void pinMode(volatile uint8_t *ddr, uint8_t bit, uint8_t direction) {

    if (direction == INPUT) {
        *ddr &= ~(1<<(bit));
    } else {
        *ddr |= (1<<(bit));
    }

}

static inline void digitalWrite(volatile uint8_t *port, uint8_t bit, uint8_t state) {
    if (state == LOW) {
        *port &= ~(1<<(bit));
    } else {
        *port |= (1<<(bit));
    }
}

static inline uint8_t digitalRead(volatile uint8_t *pin, uint8_t bit) {
    return ((*pin) & (1<<(bit)))? 1:0;
}
