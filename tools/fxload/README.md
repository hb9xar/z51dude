# fxload

fxload utility that allows uploading firmware images to Cypress 
EZ-USB devices.

This version of fxload has been anhanced to support writing VID/PID 
values to the external I2C EEPROM of the Cypress device, as well as
reading and dumping the content of this I2C EEPROM.

proudly cloned from https://github.com/esden/fxload.git and enhanced
to support reading the internal EEPROM.


* find proper USB device
  $ lsusb 
  [...]
  Bus 001 Device 125: ID 1a29:aa00  
  [...]

* read the first 64 bytes of the attached EEPROM
  # fxload -t fx2lp -s Vend_Ax.hex -D /dev/bus/usb/002/007 -r 64

* set PID/VID (ABOV OCS adapter: 1a29:aa00)
  Note: Configuration byte (-c 0x00) is 0x00
  # fxload -t fx2lp -s Vend_Ax.hex -D /dev/bus/usb/002/007 -d 1a29:aa00 -c 0x00
  [-c config_byte] [-d VID:PID]
