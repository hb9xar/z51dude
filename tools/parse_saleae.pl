#!/usr/bin/perl
#
#    z51dude
#    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

# Simple helper that reads some Saleae Decoder CSV export and
# translates known sequences to human readble text.

undef $/;

while (<>) {
    # read CPU-ID
    s/\n[^\n]*Cmd:0x22[^\n]*\n[^\n]*Tgt:0x20[^\n]*\n[^\n]*AddrL:0x00[^\n]*\n[^\n]*AddrM:0x00[^\n]*\n[^\n]*AddrH:0x00[^\n]*\n[^\n]*Cnt:0x02[^\n]*\n[^\n]*0x..[^\n]*\n[^\n]*0x..[^\n]*/\n[idle] readCpuId/gs;

    # read BDC 0x03
    s/\n[^\n]*Cmd:0x22[^\n]*\n[^\n]*Tgt:0x20[^\n]*\n[^\n]*AddrL:0x03[^\n]*\n[^\n]*AddrM:0x00[^\n]*\n[^\n]*AddrH:0x00[^\n]*\n[^\n]*Cnt:0x01[^\n]*\n[^\n]*0x48[^\n]*/\n[idle] readBDC-0x03/gs;

    # read BDC 0x05
    s/\n[^\n]*Cmd:0x22[^\n]*\n[^\n]*Tgt:0x20[^\n]*\n[^\n]*AddrL:0x05[^\n]*\n[^\n]*AddrM:0x00[^\n]*\n[^\n]*AddrH:0x00[^\n]*\n[^\n]*Cnt:0x01[^\n]*\n[^\n]*0x00[^\n]*/\n[idle] readBDC-0x05/gs;

    # write BDC 0x00
    s/\n[^\n]*Cmd:0x12[^\n]*\n[^\n]*Tgt:0x20[^\n]*\n[^\n]*AddrL:0x00[^\n]*\n[^\n]*AddrM:0x00[^\n]*\n[^\n]*AddrH:0x00[^\n]*\n[^\n]*0x00[^\n]*/\n[idle] writeBDC-0x00/gs;

    # write OCD Reset Sequence
    s/\n[^\n]*Cmd:0x12[^\n]*\n[^\n]*Tgt:0x21[^\n]*\n[^\n]*AddrL:0x01[^\n]*\n[^\n]*AddrM:0x00[^\n]*\n[^\n]*AddrH:0x00[^\n]*\n[^\n]*0x80[^\n]*/\nOCD-Reset(1)/gs;
    s/\n[^\n]*Cmd:0x12[^\n]*\n[^\n]*Tgt:0x21[^\n]*\n[^\n]*AddrL:0x00[^\n]*\n[^\n]*AddrM:0x00[^\n]*\n[^\n]*AddrH:0x00[^\n]*\n[^\n]*0x90[^\n]*/\nOCD-Reset(2)/gs;
    s/\n[^\n]*Cmd:0x12[^\n]*\n[^\n]*Tgt:0x21[^\n]*\n[^\n]*AddrL:0x01[^\n]*\n[^\n]*AddrM:0x00[^\n]*\n[^\n]*AddrH:0x00[^\n]*\n[^\n]*0x00[^\n]*/\nOCD-Reset(3)/gs;

    print;
}

