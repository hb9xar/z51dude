/*
 * This Saleae Analyzer is based on the Saleae I2C Analyzer Code which is 
 * available from <https://support.saleae.com/saleae-api-and-sdk/protocol-analyzer-sdk>.
 * The modifications for capturing, decoding and analyzing the ABOV OCD protocol have
 * been made by Thomas Ries <tries@gmx.net>.
 */

#include "ABOVAnalyzerResults.h"
#include <AnalyzerHelpers.h>
#include "ABOVAnalyzer.h"
#include "ABOVAnalyzerSettings.h"
#include <iostream>
#include <sstream>
#include <stdio.h>

ABOVAnalyzerResults::ABOVAnalyzerResults( ABOVAnalyzer* analyzer, ABOVAnalyzerSettings* settings )
:	AnalyzerResults(),
	mSettings( settings ),
	mAnalyzer( analyzer )
{
}

ABOVAnalyzerResults::~ABOVAnalyzerResults()
{
}

void ABOVAnalyzerResults::GenerateBubbleText( U64 frame_index, Channel& /*channel*/, DisplayBase display_base )  //unrefereced vars commented out to remove warnings.
{
	//we only need to pay attention to 'channel' if we're making bubbles for more than one channel (as set by AddChannelBubblesWillAppearOn)
	ClearResultStrings();
	Frame frame = GetFrame( frame_index );

	char ack[32];
	if( ( frame.mFlags & ABOV_FLAG_ACK ) != 0 )
		snprintf( ack, sizeof(ack), "ACK" );
	else if( ( frame.mFlags & ABOV_MISSING_FLAG_ACK ) != 0 )
		snprintf( ack, sizeof( ack ), "Missing ACK/NAK" );
	else
		snprintf( ack, sizeof(ack), "NAK" );

	char number_str[128];
	AnalyzerHelpers::GetNumberString( frame.mData1, display_base, 8, number_str, 128 );

	AddResultString( number_str );

	std::stringstream ss;

	if( frame.mType == ABOVCommand ) {
		ss << "Cmd: " << number_str << " [" << lookupCmd(frame.mData1) << "] + " << ack;
	} else if (frame.mType == ABOVTarget) {
		ss << "Tgt: " << number_str << " [" << lookupTgt(frame.mData1) << "] + " << ack;
	} else if (frame.mType == ABOVAddressL) {
		ss << "AdL: " << number_str << " + " << ack;
	} else if (frame.mType == ABOVAddressM) {
		ss << "AdM: " << number_str << " + " << ack;
	} else if (frame.mType == ABOVAddressH) {
		ss << "AdH: " << number_str << " + " << ack;
	} else if (frame.mType == ABOVCount) {
		ss << "Cnt: " << number_str << " + " << ack;
	} else if (frame.mType == ABOVDummy) {
		ss << "Dummy: " << number_str << " + " << ack;
	} else {
		ss << number_str << " + " << ack;
	}

	AddResultString( ss.str().c_str() );

}

void ABOVAnalyzerResults::GenerateExportFile( const char* file, DisplayBase display_base, U32 /*export_type_user_id*/ )
{
	//export_type_user_id is only important if we have more than one export type.

	std::stringstream ss;
	void* f = AnalyzerHelpers::StartFile( file );;

	U64 trigger_sample = mAnalyzer->GetTriggerSample();
	U32 sample_rate = mAnalyzer->GetSampleRate();

	ss << "Time [s],Packet ID,Address,Data,Read/Write,ACK/NAK" << std::endl;

	char address[128] = "";
	char rw[128] = "";
	U64 num_frames = GetNumFrames();
	for( U32 i=0; i < num_frames; i++ )
	{
		Frame frame = GetFrame( i );

		char ack[32];
		if( ( frame.mFlags & ABOV_FLAG_ACK ) != 0 )
			snprintf( ack, sizeof(ack), "ACK" );
		else if( ( frame.mFlags & ABOV_MISSING_FLAG_ACK ) != 0 )
			snprintf( ack, sizeof( ack ), "Missing ACK/NAK" );
		else
			snprintf( ack, sizeof(ack), "NAK" );

		// ABOVData
		char time[128];
		AnalyzerHelpers::GetTimeString( frame.mStartingSampleInclusive, trigger_sample, sample_rate, time, 128 );
		char data[128];
		AnalyzerHelpers::GetNumberString( frame.mData1, display_base, 8, data, 128);

		U64 packet_id = GetPacketContainingFrameSequential( i ); 
		if( packet_id != INVALID_RESULT_INDEX )
			ss << time << "," << packet_id << "," << address << "," << data << "," << rw << "," << ack << std::endl;
		else
			ss << time << ",," << address << "," << data << "," << rw << "," << ack << std::endl;

		AnalyzerHelpers::AppendToFile( (U8*)ss.str().c_str(), ss.str().length(), f );
		ss.str( std::string() );
					
					
		if( UpdateExportProgressAndCheckForCancel( i, num_frames ) == true )
		{
			AnalyzerHelpers::EndFile( f );
			return;
		}
	}

	UpdateExportProgressAndCheckForCancel( num_frames, num_frames );
	AnalyzerHelpers::EndFile( f );
}

void ABOVAnalyzerResults::GenerateFrameTabularText( U64 frame_index, DisplayBase display_base )
{
    ClearTabularText();

	Frame frame = GetFrame( frame_index );

	char ack[32];
	if( ( frame.mFlags & ABOV_FLAG_ACK ) != 0 )
		snprintf( ack, sizeof(ack), "ACK" );
	else if( ( frame.mFlags & ABOV_MISSING_FLAG_ACK ) != 0 )
		snprintf( ack, sizeof( ack ), "Missing ACK/NAK" );
	else
		snprintf( ack, sizeof(ack), "NAK" );

		char number_str[128];
		AnalyzerHelpers::GetNumberString( frame.mData1, display_base, 8, number_str, 128 );
		std::stringstream ss;
	if( frame.mType == ABOVCommand ) {
		ss << "Cmd:" << number_str << " [" << lookupCmd(frame.mData1) << "] + " << ack;
	} else if (frame.mType == ABOVTarget) {
		ss << "Tgt:" << number_str << " [" << lookupTgt(frame.mData1) << "] + " << ack;
	} else if (frame.mType == ABOVAddressL) {
		ss << "AddrL:" << number_str << " + " << ack;
	} else if (frame.mType == ABOVAddressM) {
		ss << "AddrM:" << number_str << " + " << ack;
	} else if (frame.mType == ABOVAddressH) {
		ss << "AddrH:" << number_str << " + " << ack;
	} else if (frame.mType == ABOVCount) {
		ss << "Cnt:" << number_str << " + " << ack;
	} else if (frame.mType == ABOVDummy) {
		ss << "Dummy:" << number_str << " + " << ack;
	} else {
		// ABOVData
		ss << number_str << " + " << ack;
	}

	AddTabularText( ss.str().c_str() );

}

void ABOVAnalyzerResults::GeneratePacketTabularText( U64 /*packet_id*/, DisplayBase /*display_base*/ )  //unrefereced vars commented out to remove warnings.
{
	ClearResultStrings();
	AddResultString( "not supported" );
}

void ABOVAnalyzerResults::GenerateTransactionTabularText( U64 /*transaction_id*/, DisplayBase /*display_base*/ )  //unrefereced vars commented out to remove warnings.
{
	ClearResultStrings();
	AddResultString( "not supported" );
}

const char* ABOVAnalyzerResults::lookupCmd( U64 value ) const {
	const char* ptr=NULL;
	switch (value) {
		case (0x10): ptr="WriteNext";break;
		case (0x12): ptr="Write";break;
		case (0x20): ptr="ReadNext";break;
		case (0x22): ptr="Read";break;
		default:     ptr="**unknown**"; break;
	}
	return ptr;
}

const char* ABOVAnalyzerResults::lookupTgt( U64 value ) const {
	const char* ptr=NULL;
	switch (value) {
		case (0x10): ptr="CODE";break;
		case (0x11): ptr="XDATA";break;
		case (0x12): ptr="IRAM";break;
		case (0x13): ptr="SFR";break;
		case (0x20): ptr="BDC";break;
		case (0x21): ptr="DBG";break;
		case (0x22): ptr="TRG";break;
		default:     ptr="**unknown**"; break;
	}
	return ptr;
}
