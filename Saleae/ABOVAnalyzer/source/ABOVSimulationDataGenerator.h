﻿/*
 * This Saleae Analyzer is based on the Saleae I2C Analyzer Code which is 
 * available from <https://support.saleae.com/saleae-api-and-sdk/protocol-analyzer-sdk>.
 * The modifications for capturing, decoding and analyzing the ABOV OCD protocol have
 * been made by Thomas Ries <tries@gmx.net>.
 */

#ifndef SERIAL_SIMULATION_DATA_GENERATOR
#define SERIAL_SIMULATION_DATA_GENERATOR

#include <AnalyzerHelpers.h>
#include "ABOVAnalyzerSettings.h"
#include <stdlib.h>

class ABOVSimulationDataGenerator
{
public:
	ABOVSimulationDataGenerator();
	~ABOVSimulationDataGenerator();

	void Initialize( U32 simulation_sample_rate, ABOVAnalyzerSettings* settings );
	U32 GenerateSimulationData( U64 newest_sample_requested, U32 sample_rate, SimulationChannelDescriptor** simulation_channels );

protected:
	ABOVAnalyzerSettings* mSettings;
	U32 mSimulationSampleRateHz;
	U8 mValue;

protected:	//ABOV specific
			//functions
	void CreateABOVTransaction( U8 address, U8 data );
	void CreateABOVByte( U8 data, ABOVResponse reply );
	void CreateBit( BitState bit_state );
	void CreateStart();
	void CreateStop();
	void CreateRestart();
	void SafeChangeSda( BitState bit_state );
	void CreateStartSequence();

protected: //vars
	ClockGenerator mClockGenerator;

	SimulationChannelDescriptorGroup mABOVSimulationChannels;
	SimulationChannelDescriptor* mSda;
	SimulationChannelDescriptor* mScl;
};
#endif //UNIO_SIMULATION_DATA_GENERATOR
