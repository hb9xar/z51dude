/*
 * This Saleae Analyzer is based on the Saleae I2C Analyzer Code which is 
 * available from <https://support.saleae.com/saleae-api-and-sdk/protocol-analyzer-sdk>.
 * The modifications for capturing, decoding and analyzing the ABOV OCD protocol have
 * been made by Thomas Ries <tries@gmx.net>.
 */

#ifndef SERIAL_ANALYZER_RESULTS
#define SERIAL_ANALYZER_RESULTS

#include <AnalyzerResults.h>

#define ABOV_FLAG_ACK ( 1 << 0 )
#define ABOV_MISSING_FLAG_ACK ( 1 << 1 )

enum ABOVFrameType { ABOVCommand, ABOVTarget, ABOVAddressL, ABOVAddressM, ABOVAddressH, ABOVCount, ABOVData, ABOVDummy };

class ABOVAnalyzer;
class ABOVAnalyzerSettings;

class ABOVAnalyzerResults : public AnalyzerResults
{
public:
	ABOVAnalyzerResults( ABOVAnalyzer* analyzer, ABOVAnalyzerSettings* settings );
	virtual ~ABOVAnalyzerResults();

	virtual void GenerateBubbleText( U64 frame_index, Channel& channel, DisplayBase display_base );
	virtual void GenerateExportFile( const char* file, DisplayBase display_base, U32 export_type_user_id );

	virtual void GenerateFrameTabularText(U64 frame_index, DisplayBase display_base );
	virtual void GeneratePacketTabularText( U64 packet_id, DisplayBase display_base );
	virtual void GenerateTransactionTabularText( U64 transaction_id, DisplayBase display_base );

protected: //functions
	virtual const char* lookupCmd( U64 value ) const;
	virtual const char* lookupTgt( U64 value ) const;

protected:  //vars
	ABOVAnalyzerSettings* mSettings;
	ABOVAnalyzer* mAnalyzer;
};

#endif //SERIAL_ANALYZER_RESULTS
