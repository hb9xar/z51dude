/*
 * This Saleae Analyzer is based on the Saleae I2C Analyzer Code which is 
 * available from <https://support.saleae.com/saleae-api-and-sdk/protocol-analyzer-sdk>.
 * The modifications for capturing, decoding and analyzing the ABOV OCD protocol have
 * been made by Thomas Ries <tries@gmx.net>.
 */

#include "ABOVAnalyzerSettings.h"

#include <AnalyzerHelpers.h>
#include <cstring>

ABOVAnalyzerSettings::ABOVAnalyzerSettings()
:	mSclChannel( UNDEFINED_CHANNEL ),
	mSdaChannel( UNDEFINED_CHANNEL )
{

	mSclChannelInterface.reset( new AnalyzerSettingInterfaceChannel() );
	mSclChannelInterface->SetTitleAndTooltip( "SCL", "Serial Clock Line" );
	mSclChannelInterface->SetChannel( mSclChannel );

	mSdaChannelInterface.reset( new AnalyzerSettingInterfaceChannel() );
	mSdaChannelInterface->SetTitleAndTooltip( "SDA", "Serial Data Line" );
	mSdaChannelInterface->SetChannel( mSdaChannel );

	AddInterface( mSclChannelInterface.get() );
	AddInterface( mSdaChannelInterface.get() );

	//AddExportOption( 0, "Export as text/csv file", "text (*.txt);;csv (*.csv)" );
	AddExportOption( 0, "Export as text/csv file" );
	AddExportExtension( 0, "text", "txt" );
	AddExportExtension( 0, "csv", "csv" );

	ClearChannels();
	AddChannel( mSclChannel, "SCL", false );
	AddChannel( mSdaChannel, "SDA", false );
}

ABOVAnalyzerSettings::~ABOVAnalyzerSettings()
{
}

bool ABOVAnalyzerSettings::SetSettingsFromInterfaces()
{
	if( mSdaChannelInterface->GetChannel() == mSclChannelInterface->GetChannel() )
	{
		SetErrorText( "SDA and SCL can't be assigned to the same input." );
		return false;
	}

	mSclChannel = mSclChannelInterface->GetChannel();
	mSdaChannel = mSdaChannelInterface->GetChannel();

	ClearChannels();
	AddChannel( mSclChannel, "SCL", true );
	AddChannel( mSdaChannel, "SDA", true );

	return true;
}

void ABOVAnalyzerSettings::LoadSettings( const char* settings )
{
	SimpleArchive text_archive;
	text_archive.SetString( settings );

	const char* name_string;	//the first thing in the archive is the name of the protocol analyzer that the data belongs to.
	text_archive >> &name_string;
	if( strcmp( name_string, "ABOVAnalyzer" ) != 0 )
		AnalyzerHelpers::Assert( "ABOVAnalyzer: Provided with a settings string that doesn't belong to us;" );

	text_archive >> mSclChannel;
	text_archive >> mSdaChannel;

	ClearChannels();
	AddChannel( mSclChannel, "SCL", true );
	AddChannel( mSdaChannel, "SDA", true );

	UpdateInterfacesFromSettings();
}

const char* ABOVAnalyzerSettings::SaveSettings()
{
	SimpleArchive text_archive;

	text_archive << "ABOVAnalyzer";
	text_archive << mSclChannel;
	text_archive << mSdaChannel;

	return SetReturnString( text_archive.GetString() );
}

void ABOVAnalyzerSettings::UpdateInterfacesFromSettings()
{
	mSclChannelInterface->SetChannel( mSclChannel );
	mSdaChannelInterface->SetChannel( mSdaChannel );
}
