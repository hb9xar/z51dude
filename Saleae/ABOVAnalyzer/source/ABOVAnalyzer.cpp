/*
 * This Saleae Analyzer is based on the Saleae I2C Analyzer Code which is 
 * available from <https://support.saleae.com/saleae-api-and-sdk/protocol-analyzer-sdk>.
 * The modifications for capturing, decoding and analyzing the ABOV OCD protocol have
 * been made by Thomas Ries <tries@gmx.net>.
 */

#include "ABOVAnalyzer.h"
#include "ABOVAnalyzerSettings.h"
#include <AnalyzerChannelData.h>

ABOVAnalyzer::ABOVAnalyzer()
:	Analyzer2(),  
	mSettings( new ABOVAnalyzerSettings() ),
	mSimulationInitilized( false )
{
	SetAnalyzerSettings( mSettings.get() );
}

ABOVAnalyzer::~ABOVAnalyzer()
{
	KillThread();
}

void ABOVAnalyzer::SetupResults()
{
	mResults.reset( new ABOVAnalyzerResults( this, mSettings.get() ) );
	SetAnalyzerResults( mResults.get() );
	mResults->AddChannelBubblesWillAppearOn( mSettings->mSdaChannel );
}

void ABOVAnalyzer::WorkerThread()
{
	mSampleRateHz = GetSampleRate();
	mByteNumber=0;

	mSda = GetAnalyzerChannelData( mSettings->mSdaChannel );
	mScl = GetAnalyzerChannelData( mSettings->mSclChannel );

	AdvanceToStartBit(); 
	mScl->AdvanceToNextEdge(); //now scl is low.

	for( ; ; )
	{
		GetByte();
		CheckIfThreadShouldExit();
	}
}

void ABOVAnalyzer::GetByte()
{
	mArrowLocataions.clear();
	U64 value;
	DataBuilder byte;
	byte.Reset( &value, AnalyzerEnums::LsbFirst, 8 );
	U64 starting_stample = 0;
	U64 potential_ending_sample = 0;
	
	for( U32 i=0; i<8; i++ )
	{
		BitState bit_state;
		U64 scl_rising_edge;
		bool result = GetBitPartOne( bit_state, scl_rising_edge, potential_ending_sample );
		result &= GetBitPartTwo();
		if( result == true )
		{
			mArrowLocataions.push_back( scl_rising_edge );
			byte.AddBit( bit_state );

			if( i == 0 )
				starting_stample = scl_rising_edge;
		}else
		{
			return;
		}
	}

	BitState parity_bit_state;
	U64 scl_rising_edge;

	bool result1 = GetBitPartOne( parity_bit_state, scl_rising_edge, potential_ending_sample );//GetBit( parity bit, scl_rising_edge );
	result1 &= GetBitPartTwo();
	if( result1 == true ) {
		mArrowLocataions.push_back( scl_rising_edge );
	} else { 
		return; 
	}

	BitState ack_bit_state;
	S64 last_valid_sample = mScl->GetSampleNumber();
	bool result = GetBitPartOne( ack_bit_state, scl_rising_edge, potential_ending_sample );//GetBit( ack_bit_state, scl_rising_edge );
	
	Frame frame;
	frame.mStartingSampleInclusive = starting_stample;
	frame.mEndingSampleInclusive = result ? potential_ending_sample : last_valid_sample;
	frame.mData1 = U8( value );

	if( !result )
		frame.mFlags = ABOV_MISSING_FLAG_ACK;
	else if( ack_bit_state == BIT_HIGH )
		frame.mFlags = DISPLAY_AS_WARNING_FLAG;
	else
		frame.mFlags = ABOV_FLAG_ACK;

	if (mByteNumber == 0) {
		frame.mType = ABOVCommand;
		mCurrentCommand=value;
		// special case: ReadNext -> next field is length field
		if (mCurrentCommand == 0x20) { mByteNumber = 5-1; }
		// special case: WriteNext -> next field is data
		if (mCurrentCommand == 0x10) { mByteNumber = 5-1; }
	} else if (mByteNumber == 1) {
		frame.mType = ABOVTarget;
		mCurrentTarget=value;
	} else if (mByteNumber == 2) {
		frame.mType = ABOVAddressL;
	} else if (mByteNumber == 3) {
		frame.mType = ABOVAddressM;
	} else if (mByteNumber == 4) {
		frame.mType = ABOVAddressH;
	} else if (mByteNumber == 5) {
		if ((mCurrentCommand == 0x22 /* Read */) ||
			(mCurrentCommand == 0x20 /* ReadNext */) ) {
			// read* -> requires a length field
			frame.mType = ABOVCount;
//		} else if (mCurrentCommand == 0x10) {
//			// WriteNext required a dummy count field
//			frame.mType = ABOVDummy;
		} else {
			frame.mType = ABOVData;
		}

	} else {
		frame.mType = ABOVData;
	}

	mResults->AddFrame( frame );

	U32 count = mArrowLocataions.size();
	for( U32 i=0; i<count; i++ )
		mResults->AddMarker( mArrowLocataions[i], AnalyzerResults::UpArrow, mSettings->mSclChannel );

	mResults->CommitResults();

	result &= GetBitPartTwo();

	mByteNumber++;
}

bool ABOVAnalyzer::GetBitPartOne( BitState& bit_state, U64& sck_rising_edge, U64& frame_end_sample )
{
	//SCL must be low coming into this function
	mScl->AdvanceToNextEdge(); //posedge
	sck_rising_edge = mScl->GetSampleNumber();
	frame_end_sample = sck_rising_edge;
	mSda->AdvanceToAbsPosition( sck_rising_edge );  //data read on SCL posedge

	bit_state = mSda->GetBitState();

	//clock is on the rising edge, and data is at the same location.

//&&&
//mResults->AddMarker( mScl->GetSampleNumber(), AnalyzerResults::Square, mSettings->mSclChannel );
	while( mScl->DoMoreTransitionsExistInCurrentData() == false )
	{
		// there are no more SCL transtitions, at least yet.
		if( mSda->DoMoreTransitionsExistInCurrentData() == true )
		{
			//there ARE some SDA transtions, let's double check to make sure there's still no SDA activity
			if( mScl->DoMoreTransitionsExistInCurrentData() == true )
			{
				if( mScl->GetSampleOfNextEdge() < mSda->GetSampleOfNextEdge() )
					break; //there is not a stop or start condition here, everything is normal - we should jump out now.
			}
			

//&&&
//mResults->AddMarker( mScl->GetSampleNumber(), AnalyzerResults::Dot, mSettings->mSclChannel );
			//ok, for sure we can advance to the next SDA edge without running past any SCL events.
			mSda->AdvanceToNextEdge();
			mScl->AdvanceToAbsPosition( mSda->GetSampleNumber() ); //clock is still high, we're just moving it to the stop condition here.
			RecordStartStopBit();
			return false;
		}
	}

	//ok, so there are more transitions on the clock channel, so the above code path didn't run.
	U64 sample_of_next_clock_falling_edge = mScl->GetSampleOfNextEdge();
	while( mSda->WouldAdvancingToAbsPositionCauseTransition( sample_of_next_clock_falling_edge - 1 ) == true )
	{
//&&&
//mResults->AddMarker( mScl->GetSampleNumber(), AnalyzerResults::X, mSettings->mSclChannel );
		//clock is high -- SDA changes indicate start, stop, etc.
		mSda->AdvanceToNextEdge();
		mScl->AdvanceToAbsPosition( mSda->GetSampleNumber() ); //advance the clock to match the SDA channel.
		RecordStartStopBit();
		return false;
	}

	if( mScl->DoMoreTransitionsExistInCurrentData() == true )
	{
		frame_end_sample = mScl->GetSampleOfNextEdge();
	}

	return true;
	
}

bool ABOVAnalyzer::GetBitPartTwo()
{
	//the sda and scl should be synced up, and we are either on a stop/start condition (clock high) or we're on a regular bit( clock high).
	//we also should not expect any more start/stop conditions before the next falling edge, I beleive.

	//move to next falling edge.
	bool result = true;
	mScl->AdvanceToNextEdge();
	while( mSda->WouldAdvancingToAbsPositionCauseTransition( mScl->GetSampleNumber() - 1 ) == true )
	{
		//clock is high -- SDA changes indicate start, stop, etc.
		mSda->AdvanceToNextEdge();
		RecordStartStopBit();
		result = false;
	}
	return result;
}

void ABOVAnalyzer::RecordStartStopBit()
{
	if( mSda->GetBitState() == BIT_LOW )
	{
		//negedge -> START / restart
		mResults->AddMarker( mSda->GetSampleNumber(), AnalyzerResults::Start, mSettings->mSdaChannel );
		mByteNumber=0;

	}else
	{
		//posedge -> STOP
		mResults->AddMarker( mSda->GetSampleNumber(), AnalyzerResults::Stop, mSettings->mSdaChannel );
	}

	mResults->CommitPacketAndStartNewPacket();
	mResults->CommitResults( );

}

void ABOVAnalyzer::AdvanceToStartBit()
{
	for( ; ; )
	{
		mSda->AdvanceToNextEdge();

		if( mSda->GetBitState() == BIT_LOW )
		{
			//SDA negedge
			mScl->AdvanceToAbsPosition( mSda->GetSampleNumber() );
			if( mScl->GetBitState() == BIT_HIGH )
				break;
		}	
	}
	mResults->AddMarker( mSda->GetSampleNumber(), AnalyzerResults::Start, mSettings->mSdaChannel );
	mByteNumber=0;
}

bool ABOVAnalyzer::NeedsRerun()
{
	return false;
}

U32 ABOVAnalyzer::GenerateSimulationData( U64 minimum_sample_index, U32 device_sample_rate, SimulationChannelDescriptor** simulation_channels )
{
	if( mSimulationInitilized == false )
	{
		mSimulationDataGenerator.Initialize( GetSimulationSampleRate(), mSettings.get() );
		mSimulationInitilized = true;
	}

	return mSimulationDataGenerator.GenerateSimulationData( minimum_sample_index, device_sample_rate, simulation_channels );
}

U32 ABOVAnalyzer::GetMinimumSampleRateHz()
{
	return 2000000;
}

const char* ABOVAnalyzer::GetAnalyzerName() const
{
	return "ABOV OCD";
}

const char* GetAnalyzerName()
{
	return "ABOV OCD";
}

Analyzer* CreateAnalyzer()
{
	return new ABOVAnalyzer();
}

void DestroyAnalyzer( Analyzer* analyzer )
{
	delete analyzer;
}
