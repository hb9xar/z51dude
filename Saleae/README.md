ABOVAnalyzer
------------
This directory contains an analyzer plug-in for the Saleae logic analyzer. It
decodes the communication between an ABOV/Z51 OCD programmer and the MCU target
(CLK and DATA).


Build Instructions (Linux)
--------------------------
You need to download the Saleae SDK located at github 
<https://github.com/saleae/AnalyzerSDK.git> and place it /copy or symlink it)
into this directory. Note that you need to prepare the Analyzer SDK according
to the documentation provided in there.

The directory structure should then look like:
./ABOVAnalyzer
./AnalyzerSDK -> /path/to/github/AnalyzerSDK

Now, build the analyzer plug-in
$ cd ABOVAnalyzer
$ python  ./build_analyzer.py

Then, copy the resulting shared library libABOVAnalyzer.so to your Logic 
plug-in directory and start the Analyzer. Look for the new Plugin called 
"ABOV OCD".

