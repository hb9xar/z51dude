This Saleae Analyzer is based on the Saleae I2C Analyzer Code which is 
available from <https://support.saleae.com/saleae-api-and-sdk/protocol-analyzer-sdk>.
The modifications for capturing, decoding and analyzing the ABOV OCD protocol 
have been made by Thomas Ries <tries@gmx.net>.

Link to Saleae License:
<https://support.saleae.com/faq/general-faq/what-is-the-license-on-the-saleae-software-and-sdk-where-can-this-be-found>

As Saleae does not use an open source license on their SDK and Analyzers
this makes things a bit complicated.

<https://support.saleae.com/faq/technical-faq/saleae-open-source-support>

"Right now, you can download the source code for the Saleae protocol 
analyzers. They are licensed under the same license included with the 
software and should only be used to develop new analyzers for the Saleae 
software and logic analyzers, not other logic analyzers."

However, my understanding of the above statement is, they do explicitely 
allow the creation of new protocol analyzers based  on their Analyzer code 
(as long as it only does support Saleae logic  analyzers).

