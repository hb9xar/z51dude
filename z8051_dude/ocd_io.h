/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

//***
//*** macros
//***


//***
//*** constants
//***
#define CMD_ECHO              0x00
#define CMD_RESET_PWR_ARDUINO 0x01
#define CMD_RESET_PWR_EXTERN  0x02
#define CMD_Z8051_CMD         0x03

#define SERTIMEOUT            5 // timeout in seconds

#define BOOTLOADER_OPTIBOOT   1
#define BOOTLOADER_ATMEGABOOT 2


//***
//*** prototypes
//***
int send_data_frame(int , unsigned char* , unsigned char );
int recv_data_frame(int , unsigned char* , unsigned char );
int set_ar_attr(int , int );
int bl_hack_fast_startup(int fd, int bootloader);
