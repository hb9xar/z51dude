/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>

//***
//*** global variables
//***


//***
//*** code
//***

int print_hex(unsigned char *buff, int size, unsigned int baseaddr) {
   int i;
   for(i=0; i<size;i++) {
      if (i%16) {
         printf(" %02x",buff[i]); 
      } else {
         if( i != 0 ) {
            printf("\n");
         } 
         printf("0x%04x  %02x",baseaddr+i, buff[i]); 
      }
   }
   printf("\n");

   return 0;
}
 

int parse_addr_value_arg(char *arg, int *address, int *value) {
   int ret;
   int sts=0;

   if (arg) {
      ret=sscanf(arg, "0x%04x=0x%02x", address, value);
      if (ret != 2) {
         // failed to parse optarg
         printf("failed to parse '%s'\n", arg);
         sts=-1;
      }
   } else {
      printf("NULL argument passed\n");
      sts=-2;
   }
   return sts;
}
