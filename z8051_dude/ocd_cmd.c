/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "z8051_dev.h"
#include "ocd_io.h"
#include "ocd_cmd.h"
#include "ocd.h"

#include "intelhex.h"
#include "utils.h"


//***
//*** global variables
//***
unsigned char wrbuffer[MAXBUF];
unsigned char rdbuffer[MAXBUF];


//***
//*** static functions
//***
static int cmd_z8051_pgm_page(int , unsigned char* , unsigned int );
static int cmd_z8051_option_page_erase(int fd, int clear_prot);
static int cmd_z8051_option_page_write(int fd, unsigned char *buffer);


//***
//*** code
//***

int cmd_echo(int fd) {
   int ret;
 
   ret=send_data_frame(fd, ocd_cmd_echo, sizeof(ocd_cmd_echo));
   printf("cmd_echo: send echo\n");
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   if(ret>=0) {
      printf("received echo\n");
   } else {
      printf("communication not ok returned=%d\n", ret);
   }

   return ret;
}


int cmd_z8051_reset(int fd, unsigned char reset_type) {
   int ret;
   int i;

   wrbuffer[0]=reset_type; // command RESET to DBG mode

   ret=send_data_frame(fd, wrbuffer, 1);
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   for(i=0;i<3;i++) {
      printf("0x%02x ",rdbuffer[i]);
   }
   printf("\n");
   return ret;
}


int cmd_z8051_get_cpuid(int fd, int verbose) {
   int ret;
   int cpuid;
   int i;

   // needs an OCD reset
//   cmd_z8051_ocd_device_reset(fd);
   cmd_z8051_ocd_get_runstop(fd);

   ret=send_data_frame(fd, ocd_cmd_cpuid, sizeof(ocd_cmd_cpuid));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   cpuid=rdbuffer[1]+(rdbuffer[2]<<8);


   for(i=0; i<sizeof(z8051_MCU)/sizeof(z8051_MCU[0]) && z8051_MCU[i].deviceID!= cpuid;i++) { }

   if (i >= sizeof(z8051_MCU)/sizeof(z8051_MCU[0])) {
      i = -1;
   }

   if (verbose) {
      printf("cpuid: 0x%04x\n",cpuid);
      if(i >= 0) {
         printf("Device found ");
         if (z8051_MCU[i].implemented) {
            printf("and is implmented\n"); 
         } else {
            printf("and is NOT implmented\n"); 
         }
         printf("DeviceID:       0x%04x\n", z8051_MCU[i].deviceID);
         printf("DeviceName:     %s\n",     z8051_MCU[i].deviceName);
         printf("Code start addr 0x%04x\n", z8051_MCU[i].code_startAddr);
         printf("Code end addr   0x%04x\n", z8051_MCU[i].code_endAddr);
         printf("Code size       0x%04x\n", z8051_MCU[i].code_size);
         printf("XRAM start addr 0x%04x\n", z8051_MCU[i].xram_startAddr);
         printf("XRAM end addr   0x%04x\n", z8051_MCU[i].xram_endAddr);
      } else {
         printf("DevID %04x is NOT found and probably an error in communication occured\n", cpuid);
      }
   }

   return(i);
}


int cmd_z8051_ocd_device_reset(int fd) {
   int ret;
   int i;

   for (i = 0; i < sizeof(ocd_cmdseq_reset)/sizeof(ocd_cmdseq_reset[0]); i++) {
      ret=send_data_frame(fd, ocd_cmdseq_reset[i].seq, ocd_cmdseq_reset[i].length);
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   }

   return 0;
}


int cmd_z8051_ocd_get_runstop(int fd) {
   int ret;

   ret=send_data_frame(fd, ocd_cmd_getrunstop1, sizeof(ocd_cmd_getrunstop1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   return 0;
}


int cmd_z8051_read_pgm(int fd, char* filename) {
   int ret;
   int len;
   int page;
   int cpuid_idx;
   unsigned int adr;
   FILE *fp;

   adr=0;

   // needs an OCD reset (reading CODE while running gets MCU in a funny state)
   cmd_z8051_ocd_device_reset(fd);

   cpuid_idx=cmd_z8051_get_cpuid(fd, 0);

   if (cpuid_idx < 0 || z8051_MCU[cpuid_idx].deviceName == 0) {
      printf("Unknown/unsupported device, cannot read memory\n");
      return(-1);
   }

   fp = fopen(filename,"wb");
   if(fp == NULL) {
      perror("Error opening file");
      return(-1);
   }

   // first page (READ from ADDR)
   ret=send_data_frame(fd, ocd_cmd_pgm_read1, sizeof(ocd_cmd_pgm_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   len = ihex_write(&rdbuffer[1], PAGE_SIZE, adr, fp);

   // hex dump of buffer
   print_hex(&rdbuffer[1], PAGE_SIZE, adr);
   adr+=PAGE_SIZE;

   // next page (READ CONT)
   for(page=1; page < z8051_MCU[cpuid_idx].code_size/PAGE_SIZE; page++) {
      ret=send_data_frame(fd, ocd_cmd_pgm_read2, sizeof(ocd_cmd_pgm_read2));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);

      len = ihex_write(&rdbuffer[1], PAGE_SIZE, adr, fp);

      // hex dump of buffer
      print_hex(&rdbuffer[1], PAGE_SIZE, adr);
      adr+=PAGE_SIZE;
   }

   ihex_write_eof(fp);
   fclose(fp);

   return ret;
}


int cmd_z8051_write_pgm(int fd, char* filename) {
   int i;
   int len;
   int cpuid_idx;
   unsigned char buffer[PAGE_SIZE];
   int page=0;

   cpuid_idx=cmd_z8051_get_cpuid(fd, 0);
   if (cpuid_idx < 0 || z8051_MCU[cpuid_idx].deviceName == 0) {
      printf("Unknown/unsupported device, cannot write memory\n");
      return(-1);
   }

   i = ihex_load(filename);
   if (i<0) {
      printf("Error opening file\n");
      return(-1);
   }

   // needs an OCD reset (accessing CODE while running gets MCU in a funny state)
   cmd_z8051_ocd_device_reset(fd);

   do {
      len = ihex_fetch(buffer, PAGE_SIZE);
      if (len > 0) {
         //&&&printf("%02x\n",len);
         if ( len < PAGE_SIZE) {
            // fill rest with 
            for(i=len; i<PAGE_SIZE; i++) buffer[i]=0x00;
         }
         // check for upper memory limit
         if (page > z8051_MCU[cpuid_idx].code_size/PAGE_SIZE) {
            printf("\nError: trying to write outside device FLASH memory (0x%04x).\n",  page*PAGE_SIZE-1);
            return(-1);
         }

         cmd_z8051_pgm_page(fd , buffer, page);
         page++;

         printf("Programming device address: 0x%04x\r", page*PAGE_SIZE-1);
         fflush(stdout);
      }
   } while (len > 0);
   printf("\nDone.\n");
   return(0);
}


int cmd_z8051_verify_pgm(int fd, char* filename) {
   int cpuid_idx;
   int ret;
   int len;
   unsigned char buffer[PAGE_SIZE];
   int page=0;

   // needs an OCD reset (reading CODE while runnign gets MCU in a funny state)
   cmd_z8051_ocd_device_reset(fd);

   cpuid_idx=cmd_z8051_get_cpuid(fd, 0);

   if (cpuid_idx < 0 || z8051_MCU[cpuid_idx].deviceName == 0) {
      printf("Unknown/unsupported device, cannot read memory\n");
      return(-1);
   }

   ret = ihex_load(filename);
   if (ret < 0) {
      printf("Error opening file\n");
      return(-1);
   }

   // first page (READ from ADDR)
   printf("Verifying device address: 0x%04x\r", page*PAGE_SIZE);
   ret=send_data_frame(fd, ocd_cmd_pgm_read1, sizeof(ocd_cmd_pgm_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   len = ihex_fetch(buffer, PAGE_SIZE);
   if (memcmp(buffer, &rdbuffer[1], PAGE_SIZE) != 0) {
      printf("ERROR: Verify failed at address page 0x%04x\n", page*PAGE_SIZE);
      printf("Flash content:\n");
      print_hex(&rdbuffer[1], PAGE_SIZE, page*PAGE_SIZE);
      printf("File content:\n");
      print_hex(buffer, PAGE_SIZE, page*PAGE_SIZE);
      return(-1);
   }

   // next page (READ CONT)
   for(page=1; page < z8051_MCU[cpuid_idx].code_size/PAGE_SIZE; page++) {
      printf("Verifying device address: 0x%04x\r", page*PAGE_SIZE);
      fflush(stdout);

      ret=send_data_frame(fd, ocd_cmd_pgm_read2, sizeof(ocd_cmd_pgm_read2));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);

      len = ihex_fetch(buffer, PAGE_SIZE);
      // end of IHEX filereached?
      if (len == 0) { break; }
      if (memcmp(buffer, &rdbuffer[1], PAGE_SIZE) != 0) {
         printf("ERROR: Verify failed at address page 0x%04x\n", page*PAGE_SIZE);
         printf("Flash content:\n");
         print_hex(&rdbuffer[1], PAGE_SIZE, page*PAGE_SIZE);
         printf("File content:\n");
         print_hex(buffer, PAGE_SIZE, page*PAGE_SIZE);
         return(-1);
      }
   }

   printf("\nVerify OK\n");

   return(0);
}


int cmd_z8051_blankcheck(int fd) {
   int cpuid_idx;
   int ret;
   unsigned char buffer[PAGE_SIZE];
   int page=0;

   // needs an OCD reset (reading CODE while runnign gets MCU in a funny state)
   cmd_z8051_ocd_device_reset(fd);

   cpuid_idx=cmd_z8051_get_cpuid(fd, 0);

   // blank device has all 0x00
   memset(buffer, 0, PAGE_SIZE);

   if (cpuid_idx < 0 || z8051_MCU[cpuid_idx].deviceName == 0) {
      printf("Unknown/unsupported device, cannot read memory\n");
      return(-1);
   }

   // first page (READ from ADDR)
   printf("Blank checking device address: 0x%04x\r", page*PAGE_SIZE);
   ret=send_data_frame(fd, ocd_cmd_pgm_read1, sizeof(ocd_cmd_pgm_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   if (memcmp(buffer, &rdbuffer[1], PAGE_SIZE) != 0) {
      printf("ERROR: Blank check failed at page with address 0x%04x\n", page*PAGE_SIZE);
      printf("Flash content:\n");
      print_hex(&rdbuffer[1], PAGE_SIZE, page*PAGE_SIZE);
      return(-1);
   }

   // next page (READ CONT)
   for(page=1; page < z8051_MCU[cpuid_idx].code_size/PAGE_SIZE; page++) {
      printf("Blank checking device address: 0x%04x\r", page*PAGE_SIZE);
      fflush(stdout);

      ret=send_data_frame(fd, ocd_cmd_pgm_read2, sizeof(ocd_cmd_pgm_read2));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);

      if (memcmp(buffer, &rdbuffer[1], PAGE_SIZE) != 0) {
         printf("ERROR: Blank check failed at address page 0x%04x\n", page*PAGE_SIZE);
         printf("Flash content:\n");
         print_hex(&rdbuffer[1], PAGE_SIZE, page*PAGE_SIZE);
         return(-1);
      }
   }

   printf("\nDevice is blank.\n");

   return(0);
}


static int cmd_z8051_pgm_page(int fd, unsigned char * buffer, unsigned int page) {
   int ret;
   ret=send_data_frame(fd, ocd_cmd_pgm_page1, sizeof(ocd_cmd_pgm_page1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   ret=send_data_frame(fd, ocd_cmd_pgm_page2, sizeof(ocd_cmd_pgm_page2));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // Write first part of page
   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   #define SIZE_Z8051_WRITE_64 64
   memcpy(&wrbuffer[0],ocd_cmd_pgm_page3,sizeof(ocd_cmd_pgm_page3));
   memcpy(&wrbuffer[6],buffer,0x3a);

   ret=send_data_frame(fd, wrbuffer, SIZE_Z8051_WRITE_64);
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // Write second part of page
   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   #define SIZE_Z8051_WRITE_8 8
   memcpy(&wrbuffer[0],ocd_cmd_pgm_page4,sizeof(ocd_cmd_pgm_page4));
   memcpy(&wrbuffer[2],&buffer[0x3a],6);

   ret=send_data_frame(fd, wrbuffer, SIZE_Z8051_WRITE_8);
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   memcpy(wrbuffer,ocd_cmd_pgm_page5,sizeof(ocd_cmd_pgm_page5));
   wrbuffer[7]=(page*PAGE_SIZE)>>8 & 0xff; // 0xfb FSADRM
   wrbuffer[8]=(page*PAGE_SIZE) & 0xff;    // 0xfc FSADRL
   ret=send_data_frame(fd, wrbuffer, sizeof(ocd_cmd_pgm_page5));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

  // wait until finished flashing
   do {
      ret=send_data_frame(fd, ocd_cmd_pgm_page6, sizeof(ocd_cmd_pgm_page6));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   } while (rdbuffer[1]!= 0x00);
   return 0;

}


int cmd_z8051_bulk_erase(int fd) {
   int ret;

   // it is sane to reset the chip into a defined state
   cmd_z8051_ocd_device_reset(fd);

   ret=send_data_frame(fd, ocd_cmd_bulk_erase1, sizeof(ocd_cmd_bulk_erase1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   ret=send_data_frame(fd, ocd_cmd_bulk_erase2, sizeof(ocd_cmd_bulk_erase2));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   #define SIZE_Z8051_WRITE_64 64
   memcpy(wrbuffer, ocd_cmd_bulk_erase3, sizeof(ocd_cmd_bulk_erase3));
   ret=send_data_frame(fd, wrbuffer, SIZE_Z8051_WRITE_64);
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   #define SIZE_Z8051_WRITE_8 8
   memcpy(wrbuffer, ocd_cmd_bulk_erase4, sizeof(ocd_cmd_bulk_erase4));
   ret=send_data_frame(fd, wrbuffer, SIZE_Z8051_WRITE_8);
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   ret=send_data_frame(fd, ocd_cmd_bulk_erase5, sizeof(ocd_cmd_bulk_erase5));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   ret=send_data_frame(fd, ocd_cmd_bulk_erase6, sizeof(ocd_cmd_bulk_erase6));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   ret=send_data_frame(fd, ocd_cmd_bulk_erase7, sizeof(ocd_cmd_bulk_erase7));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   do {
      ret=send_data_frame(fd, ocd_cmd_bulk_erase8, sizeof(ocd_cmd_bulk_erase8));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   } while (rdbuffer[1]!= 0x00);

   printf("\nDevice erased.\n");
   return(0);
}


static int cmd_z8051_option_page_erase(int fd, int clear_prot) {
   int ret;

   ret=send_data_frame(fd, ocd_cmd_optn_erase1, sizeof(ocd_cmd_optn_erase1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   ret=send_data_frame(fd, ocd_cmd_optn_erase2, sizeof(ocd_cmd_optn_erase2));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // clear part one OCD Data Area in the range 0x8000–0x803F
   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   #define SIZE_Z8051_WRITE_64 64
   memcpy(wrbuffer, ocd_cmd_optn_erase3, sizeof(ocd_cmd_optn_erase3));
   ret=send_data_frame(fd, wrbuffer, SIZE_Z8051_WRITE_64);
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // clear part one OCD Data Area in the range 0x8000–0x803F
   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   #define SIZE_Z8051_WRITE_8 8
   memcpy(wrbuffer, ocd_cmd_optn_erase4, sizeof(ocd_cmd_optn_erase4));
   ret=send_data_frame(fd, wrbuffer, SIZE_Z8051_WRITE_8);
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   if (clear_prot == 1) {
      // clear options page including protection bits. Will erase device!
      ret=send_data_frame(fd, ocd_cmd_optn_erase5b, sizeof(ocd_cmd_optn_erase5b));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   } else {
      // just clear option page, don't erase protection bits
      ret=send_data_frame(fd, ocd_cmd_optn_erase5, sizeof(ocd_cmd_optn_erase5));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   }

   do {
      ret=send_data_frame(fd, ocd_cmd_optn_erase6, sizeof(ocd_cmd_optn_erase6));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   } while (rdbuffer[1] != 0x00);    //[7] Flash Busy Flag (0: Done; 1: Busy)

   ret=send_data_frame(fd, ocd_cmd_optn_erase7, sizeof(ocd_cmd_optn_erase7));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   return 0;
}


int cmd_z8051_option_page_read(int fd, unsigned char *pagebuff) {
   int ret;

   // write SFR(0xF9) = 0x00 ensure PGM flash is mapped to CODE
   ret=send_data_frame(fd, ocd_cmd_optn_read1, sizeof(ocd_cmd_optn_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // opt: read back and check for 0x00
   ret=send_data_frame(fd, ocd_cmd_optn_read2, sizeof(ocd_cmd_optn_read2));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // write SFR(0xF9) = 0x04 -> map OPTION page into CODE
   ret=send_data_frame(fd, ocd_cmd_optn_read3, sizeof(ocd_cmd_optn_read3));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   
   // read back twice and ensure that value is set.
   //usleep(1000);
   ret=send_data_frame(fd, ocd_cmd_optn_read4, sizeof(ocd_cmd_optn_read4));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   if (rdbuffer[1] != 0x04 ) {
      printf("Failed to map OPTIONS page\n");
      return(-1);
   }

   // CODE read from 0x00 to 0x3f
   ret=send_data_frame(fd, ocd_cmd_optn_read5, sizeof(ocd_cmd_optn_read5));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   // if buffer suplied, copy to buffer
   if (pagebuff) {
      memcpy(pagebuff, &rdbuffer[1], PAGE_SIZE);
   } else {
      // dump to stdout
      printf("Option dump:\n");
      ret=print_hex(&rdbuffer[1], PAGE_SIZE, 0x0000);
      printf("\nOption Word: 0x%04x\n", rdbuffer[0x3f + 1]<<8 | rdbuffer[0x3e + 1]);
   }

   // switch back to PGM memory
   // write SFR(0xF9) = 0x00 ensure PGM flash is mapped to CODE
   ret=send_data_frame(fd, ocd_cmd_optn_read6, sizeof(ocd_cmd_optn_read6));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // read back and ensure that value is set.
   //usleep(1000);
   ret=send_data_frame(fd, ocd_cmd_optn_read7, sizeof(ocd_cmd_optn_read7));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   if (rdbuffer[1] != 0x00 ) {
      printf("Failed to unmap OPTIONS page. Chip may need a RESET.\n");
      return(-2);
   }

   return 0;
}


static int cmd_z8051_option_page_write(int fd, unsigned char *buffer) {
   int ret;

   // write SFR(0xF9) = 0x00 ensure PGM flash is mapped to CODE
   ret=send_data_frame(fd, ocd_cmd_optn_wpage1, sizeof(ocd_cmd_optn_wpage1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // opt: read back and check for 0x00
   ret=send_data_frame(fd, ocd_cmd_optn_wpage2, sizeof(ocd_cmd_optn_wpage2));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // SFR 0cfe = 0x01 (Flash mod control reg)
   ret=send_data_frame(fd, ocd_cmd_optn_wpage3, sizeof(ocd_cmd_optn_wpage3));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // Write first part of page
   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   #define SIZE_Z8051_WRITE_64 64
   memcpy(&wrbuffer[0],ocd_cmd_optn_wpage4,sizeof(ocd_cmd_optn_wpage4));
   memcpy(&wrbuffer[6],buffer,0x3a);
   ret=send_data_frame(fd, wrbuffer, SIZE_Z8051_WRITE_64);
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // Write second part of page
   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   #define SIZE_Z8051_WRITE_8 8
   memcpy(&wrbuffer[0],ocd_cmd_optn_wpage5,sizeof(ocd_cmd_optn_wpage5));
   memcpy(&wrbuffer[2],&buffer[0x3a],6);
   ret=send_data_frame(fd, wrbuffer, SIZE_Z8051_WRITE_8);
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // write SRF(0xF9) = 0x04 -> map OPTION page into CODE
   ret=send_data_frame(fd, ocd_cmd_optn_wpage6, sizeof(ocd_cmd_optn_wpage6));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   
   // read back twice and ensure that value is set.
   //usleep(1000);
   ret=send_data_frame(fd, ocd_cmd_optn_wpage7, sizeof(ocd_cmd_optn_wpage7));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   if (rdbuffer[1] != 0x04 ) {
      printf("Failed to map OPTIONS page\n");
      return(-1);
   }

   // OPTION page is mapped to page 0 (0x00..0x3f)
   ret=send_data_frame(fd, ocd_cmd_optn_wpage8, sizeof(ocd_cmd_pgm_page5));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // wait until finished flashing
   do {
      ret=send_data_frame(fd, ocd_cmd_optn_wpage9, sizeof(ocd_cmd_optn_wpage9));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   } while (rdbuffer[1]!= 0x00);

   // switch back to PGM memory
   // write SFR(0xF9) = 0x00 ensure PGM flash is mapped to CODE
   ret=send_data_frame(fd, ocd_cmd_optn_wpage10, sizeof(ocd_cmd_optn_wpage10));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // read back and ensure that value is set.
   //usleep(1000);
   ret=send_data_frame(fd, ocd_cmd_optn_wpage11, sizeof(ocd_cmd_optn_wpage11));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   if (rdbuffer[1] != 0x00 ) {
      printf("Failed to unmap OPTIONS page. Chip may need a RESET.\n");
      return(-2);
   }

   return 0;
}


int cmd_z8051_option_byte_write(int fd, unsigned int optionbytes) {
   unsigned char pagebuff1[PAGE_SIZE];
   unsigned char pagebuff2[PAGE_SIZE];
   unsigned int old_optbytes=0;
   int clear_prot=0;
   int ret;

   memset(pagebuff1, 0x00, sizeof(pagebuff1));
   memset(pagebuff2, 0x00, sizeof(pagebuff2));

// uncomment to restore a known Option Block to the device
//#define RESTORE_OPTIONBLOCK
#ifdef RESTORE_OPTIONBLOCK
/*
* Safety copy in case of BRICK:
* My option PAG (TR)
0x0000  00 00 00 14 00 d2 00 00 5a a5 c3 00 00 00 00 00
0x0010  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
0x0020  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
0x0030  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01
*/
unsigned char mybuf[64] = {
    0x00, 0x00, 0x00, 0x14, 0x00, 0xd2, 0x00, 0x00, 0x5a, 0xa5, 0xc3, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01
};
#endif

   // it is sane to reset the chip into a defined state
   cmd_z8051_ocd_device_reset(fd);

   // read option page
   ret = cmd_z8051_option_page_read(fd, pagebuff1);
   old_optbytes = pagebuff1[0x3e] | (pagebuff1[0x3f] << 8);

   #define OPT_MASK 0xc000      // code read+write protection require device eerase
   if ( ((old_optbytes & OPT_MASK) != 0 )
     && ((old_optbytes & OPT_MASK) != (optionbytes & OPT_MASK)) ) { 
      clear_prot=1;
      printf("*** Clearing protection bits! This will erase the MCU Flash!\n");
      printf("*** You have 10 seconds to abort with CTRL-C\n");
      sleep(10);
   }

   printf("Read from device:\n");
   ret=print_hex(pagebuff1, PAGE_SIZE, 0x0000);

   // erase option page
   ret = cmd_z8051_option_page_erase(fd, clear_prot);

   // update OPTION page content (0x3e and 0x3f)
   pagebuff1[0x3e]=optionbytes & 0xff;
   pagebuff1[0x3f]=(optionbytes>>8) & 0xff;

#ifdef RESTORE_OPTIONBLOCK
   // restore known OPTION block:
   memcpy(pagebuff1, mybuf, PAGE_SIZE);
#endif

   // write option page (modified data)
   ret = cmd_z8051_option_page_write(fd, pagebuff1);

   // OCD reset (apparently not needed)

   // read & verify
   ret = cmd_z8051_option_page_read(fd, pagebuff2);
   if (memcmp(pagebuff1, pagebuff2, sizeof(pagebuff1)) != 0) {
      printf("Verification of written OPTION page failed.\n");
      printf("Should be:\n");
      ret=print_hex(pagebuff1, PAGE_SIZE, 0x0000);
      printf("Actual content:\n");
      ret=print_hex(pagebuff2, PAGE_SIZE, 0x0000);
      printf("\nYour MCU is probably toast. Try to re-flash the whole page again.\n");
      return(-2);
   }

   printf("OPTION page written and verified:\n");
   ret=print_hex(pagebuff2, PAGE_SIZE, 0x0000);
   return 0;
}


int cmd_z8051_runpgm(int fd) {
   int ret;
   int i;

   for (i = 0; i < sizeof(ocd_cmdseq_runpgm)/sizeof(ocd_cmdseq_runpgm[0]); i++) {
      ret=send_data_frame(fd, ocd_cmdseq_runpgm[i].seq, ocd_cmdseq_runpgm[i].length);
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   }

   return 0;
}


int cmd_z8051_contpgm(int fd) {
   int ret;
   int i;

   for (i = 0; i < sizeof(ocd_cmdseq_contpgm)/sizeof(ocd_cmdseq_contpgm[0]); i++) {
      ret=send_data_frame(fd, ocd_cmdseq_contpgm[i].seq, ocd_cmdseq_contpgm[i].length);
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   }

   return 0;
}


int cmd_z8051_stoppgm(int fd) {
   int ret;
   int i;

   for (i = 0; i < sizeof(ocd_cmdseq_stoppgm)/sizeof(ocd_cmdseq_stoppgm[0]); i++) {
      ret=send_data_frame(fd, ocd_cmdseq_stoppgm[i].seq, ocd_cmdseq_stoppgm[i].length);
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   }

   return 0;
}


int cmd_z8051_steppgm(int fd) {
   int ret;
   int i;

   for (i = 0; i < sizeof(ocd_cmdseq_steppgm)/sizeof(ocd_cmdseq_steppgm[0]); i++) {
      ret=send_data_frame(fd, ocd_cmdseq_steppgm[i].seq, ocd_cmdseq_steppgm[i].length);
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   }

   return 0;
}


int cmd_z8051_readsfr(int fd) {
   int ret;
   int page;
   unsigned int adr;

   // needs an OCD reset - nope, reading from DBG register seems ok.
   // cmd_z8051_ocd_device_reset(fd);
   cmd_z8051_ocd_get_runstop(fd);

   adr=SFR_START;

   // first page (READ from ADDR)
   ret=send_data_frame(fd, ocd_cmd_sfr_read1, sizeof(ocd_cmd_sfr_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   ret=print_hex(&rdbuffer[1], PAGE_SIZE, adr);
   adr+=PAGE_SIZE;

   // next page (READ CONT)
   for(page=1; page < SFR_SIZE/PAGE_SIZE; page++) {
      ret=send_data_frame(fd, ocd_cmd_sfr_read2, sizeof(ocd_cmd_sfr_read2));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);

      ret=print_hex(&rdbuffer[1], PAGE_SIZE, adr);
      adr+=PAGE_SIZE;
   }

   return(0);
}


int cmd_z8051_sfr_byte_write(int fd, int address, int value){
   int ret;
   
   if (address < SFR_START) {
      printf("invalid SFR address 0x%04x\n", address);
      return (-1);
   } 

   // needs an OCD reset - nope, reading from DBG register seems ok.
   ret=cmd_z8051_ocd_get_runstop(fd);

   // clear write buffer
   memset(wrbuffer, 0x00, sizeof(wrbuffer));

   memcpy(wrbuffer,ocd_cmd_sfr_write1,sizeof(ocd_cmd_sfr_write1));
   // put address into write request
   wrbuffer[3]= address & 0xff;
   wrbuffer[4]= (address >> 8) & 0xff;
   // put value into buffer
   wrbuffer[6] = value & 0xff;

   ret=send_data_frame(fd, wrbuffer, sizeof(ocd_cmd_sfr_write1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   return(0);
}


int cmd_z8051_readiram(int fd) {
   int ret;
   int page;
   unsigned int adr;

   // needs an OCD reset - nope, reading from DBG register seems ok.
   //cmd_z8051_ocd_device_reset(fd);
   cmd_z8051_ocd_get_runstop(fd);

   #define IRAM_SIZE 0x100
   adr=0x0000;

   // first page (READ from ADDR)
   ret=send_data_frame(fd, ocd_cmd_iram_read1, sizeof(ocd_cmd_iram_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   ret=print_hex(&rdbuffer[1], PAGE_SIZE, adr);
   adr+=PAGE_SIZE;

   // next page (READ CONT)
   for(page=1; page < IRAM_SIZE/PAGE_SIZE; page++) {
      ret=send_data_frame(fd, ocd_cmd_iram_read2, sizeof(ocd_cmd_iram_read2));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);

      ret=print_hex(&rdbuffer[1], PAGE_SIZE, adr);
      adr+=PAGE_SIZE;
   }

   return(0);
}


int cmd_z8051_iram_byte_write(int fd, int address, int value){
   int ret;
   
   if ((address < 0x0000) && (address >= IRAM_SIZE)) {
      printf("invalid IRAM address 0x%04x\n", address);
      return (-1);
   } 

   // needs an OCD reset - nope, reading from DBG register seems ok.
   //cmd_z8051_ocd_device_reset(fd);
   cmd_z8051_ocd_get_runstop(fd);

   // clear write buffer
   memset(wrbuffer, 0x00, sizeof(wrbuffer));

   memcpy(wrbuffer,ocd_cmd_iram_write1,sizeof(ocd_cmd_iram_write1));
   // put address into write request
   wrbuffer[3]= address & 0xff;
   wrbuffer[4]= (address >> 8) & 0xff;
   // put value into buffer
   wrbuffer[6] = value & 0xff;

   ret=send_data_frame(fd, wrbuffer, sizeof(ocd_cmd_iram_write1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   return(0);
}


int cmd_z8051_readxdata(int fd) {
   int ret;
   int page;
   int cpuid_idx;
   unsigned int adr;
   unsigned int start;
   unsigned int end;

   cpuid_idx=cmd_z8051_get_cpuid(fd, 0);

   if (cpuid_idx < 0 || z8051_MCU[cpuid_idx].deviceName == 0) {
      printf("Unknown/unsupported device, cannot read memory\n");
      return(-1);
   }


   start=z8051_MCU[cpuid_idx].xram_startAddr;
   end=z8051_MCU[cpuid_idx].xram_endAddr;
   if (start == end) {
      printf("Device has no XDATA\n");
      return(-1);
   }
   
   adr=start;

   // first page (READ from ADDR)
   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   memcpy(wrbuffer, ocd_cmd_xdata_read1, sizeof(ocd_cmd_xdata_read1));
   wrbuffer[3]=adr & 0xff;      // addrl
   wrbuffer[4]=(adr >> 8) & 0xff; // addrm
   ret=send_data_frame(fd, wrbuffer, sizeof(ocd_cmd_xdata_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   ret=print_hex(&rdbuffer[1], PAGE_SIZE, adr);
   adr+=PAGE_SIZE;

   // next page (READ CONT)
   for(page=1; page < (end-start+1)/PAGE_SIZE; page++) {
      ret=send_data_frame(fd, ocd_cmd_xdata_read2, sizeof(ocd_cmd_xdata_read2));
      ret=recv_data_frame(fd, rdbuffer, MAXBUF);

      ret=print_hex(&rdbuffer[1], PAGE_SIZE, adr);
      adr+=PAGE_SIZE;
   }

   return(0);
}


int cmd_z8051_xdata_byte_write(int fd, int address, int value){
   int ret;
   int cpuid_idx;
   unsigned int start;
   unsigned int end;
   
   cpuid_idx=cmd_z8051_get_cpuid(fd, 0);

   if (cpuid_idx < 0 || z8051_MCU[cpuid_idx].deviceName == 0) {
      printf("Unknown/unsupported device, cannot read memory\n");
      return(-1);
   }


   start=z8051_MCU[cpuid_idx].xram_startAddr;
   end=z8051_MCU[cpuid_idx].xram_endAddr;
   if (start == end) {
      printf("Device has no XDATA\n");
      return(-1);
   }

   if ((address < start) && (address > end)) {
      printf("invalid IRAM address 0x%04x\n", address);
      return (-1);
   } 

   //cmd_z8051_ocd_device_reset(fd); part of get_cpuid

   // clear write buffer
   memset(wrbuffer, 0x00, sizeof(wrbuffer));

   memcpy(wrbuffer,ocd_cmd_xdata_write1,sizeof(ocd_cmd_xdata_write1));
   // put address into write request
   wrbuffer[3]= address & 0xff;
   wrbuffer[4]= (address >> 8) & 0xff;
   // put value into buffer
   wrbuffer[6] = value & 0xff;

   ret=send_data_frame(fd, wrbuffer, sizeof(ocd_cmd_xdata_write1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   return(0);
}


int cmd_z8051_readreg(int fd) {
   int ret;
   unsigned int adr;
   int i;
   int psw;
   int reg_bank;

   // needs an OCD reset?
   // not really. If prg is running, registers and memory can be 
   // accessed and read.
//   cmd_z8051_ocd_device_reset(fd);

   // MCS51 registers are located at IRAM 0x00 ... 0x1F
   // 0x00 Bank 0, R0, R1...R7
   // 0x08 Bank 1, R0, R1...R7
   // 0x10 Bank 2, R0, R1...R7
   // 0x18 Bank 3, R0, R1...R7
   // PSW   0xD0
   // ACC   0xE0
   // DPTR  0x82-L 0x83-H  DPL1: 0x84-l 0x85-h
   // B     0xF0
   // PC    in OCD_DBG 0x02..0x04
   // SP    0x81

   // and yes, this function uses *hardcoded* well known IRAM and SFR 
   // addresses of the MCS51 core...


   // SFR page (0x80 ... 0xbf) -> Register banks
   adr=0x0080;
   // first page (READ from ADDR)
   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   memcpy(wrbuffer, ocd_cmd_sfr_read1, sizeof(ocd_cmd_sfr_read1));
   wrbuffer[3]=adr & 0xff;      // addrl
   wrbuffer[4]=(adr >> 8) & 0xff; // addrm
   ret=send_data_frame(fd, wrbuffer, sizeof(ocd_cmd_sfr_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // Stack pointer
   printf("SP:   %04x\n", rdbuffer[0x81-adr+1]);
   // Data Pointers
   printf("DPTR: %04x  DPTR1: %04x\n", rdbuffer[0x82-adr+1] | (rdbuffer[0x83-adr+1]<<8),
                                       rdbuffer[0x84-adr+1] | (rdbuffer[0x85-adr+1]<<8) );
   

   // SFR page (0xC0 ... 0xff) -> Register banks
   adr=0x00c0;
   // first page (READ from ADDR)
   memset(wrbuffer, 0x00, sizeof(wrbuffer));
   memcpy(wrbuffer, ocd_cmd_sfr_read1, sizeof(ocd_cmd_sfr_read1));
   wrbuffer[3]=adr & 0xff;      // addrl
   wrbuffer[4]=(adr >> 8) & 0xff; // addrm
   ret=send_data_frame(fd, wrbuffer, sizeof(ocd_cmd_sfr_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   // PSW
   psw=rdbuffer[0xd0-adr+1];
   reg_bank=(psw & 0x18)>>3;
   printf("PSW:  %02x    CY:%i AC:%i F0:%i RS1:%i RS0:%i OV:%i F1:%i P:%i\n", 
          rdbuffer[0xd0-adr+1],
          (psw & 0x80)? 1:0, 
          (psw & 0x40)? 1:0, 
          (psw & 0x20)? 1:0, 
          (psw & 0x10)? 1:0, 
          (psw & 0x08)? 1:0, 
          (psw & 0x04)? 1:0, 
          (psw & 0x02)? 1:0, 
          (psw & 0x01)? 1:0
          );
   // ACC
   printf("ACC:  %02x\n", rdbuffer[0xe0-adr+1]);
   // B
   printf("B:    %02x\n", rdbuffer[0xf0-adr+1]);

   // read PC register
   ret=send_data_frame(fd, ocd_cmd_pc_read1, sizeof(ocd_cmd_pc_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   printf("PC:   %04x\n", (rdbuffer[0x00+1] | (rdbuffer[0x01+1] << 8)));


   // first IRAM page (0x00 ... 0x3f) -> Register banks
   adr=0x0000;
   ret=send_data_frame(fd, ocd_cmd_iram_read1, sizeof(ocd_cmd_iram_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);

   printf("Registers:\n");
   for (i=0; i < 0x20; i++) {
       switch (adr+i) {
           case 0x00:
              printf ("        R0 R1 R2 R3 R4 R5 R6 R7");
           case 0x08:
           case 0x10:
           case 0x18:
              printf("\nBank%c%i: %02x ", ((adr+i)/8==reg_bank)? '*' :' ', (adr+i)/8, rdbuffer[i+1]);
              break;
           default:
              printf("%02x ", rdbuffer[i+1]);
              break;
        }
   }
   printf("\n");

   return(0);
}


int cmd_z8051_readocd(int fd) {
   int ret;
   unsigned int adr;

   // read from Background Debugging Controller
   ret=send_data_frame(fd, ocd_cmd_bdc_read1, sizeof(ocd_cmd_bdc_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   ret=send_data_frame(fd, ocd_cmd_bdc_read2, sizeof(ocd_cmd_bdc_read2));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   printf("ret=%i\n",ret);
   adr=0x0000;
   printf("Background Debugging Controller (BDC):\n");
   ret=print_hex(&rdbuffer[1], MAX(0,ret-1), adr);
   adr+=PAGE_SIZE;
   printf("\n");

   // read from Debugging Logic
   ret=send_data_frame(fd, ocd_cmd_dbg_read1, sizeof(ocd_cmd_dbg_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   adr=0x0000;
   printf("Debugging Logic (DBG):\n");
   ret=print_hex(&rdbuffer[1], MAX(0,ret-1), adr);
   adr+=PAGE_SIZE;
   printf("\n");

   // read from Debugging Trigger
   ret=send_data_frame(fd, ocd_cmd_trg_read1, sizeof(ocd_cmd_trg_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   adr=0x0000;
   printf("Debugging Trigger (TRG):\n");
   ret=print_hex(&rdbuffer[1], MAX(0,ret-1), adr);
   adr+=PAGE_SIZE;
   printf("\n");

   // read from OCD Tracer
   ret=send_data_frame(fd, ocd_cmd_trc_read1, sizeof(ocd_cmd_trc_read1));
   ret=recv_data_frame(fd, rdbuffer, MAXBUF);
   adr=0x0000;
   printf("OCD Tracer (TRACER):\n");
   ret=print_hex(&rdbuffer[1], MAX(0,ret-1), adr);
   adr+=PAGE_SIZE;
   printf("\n");

   return(0);
}


