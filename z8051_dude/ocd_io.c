/*
    z51dude
    Copyright (C) 2018-2019  Ralf Messerer, Thomas Ries

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <time.h>

#include "crc16.h"
#include "ocd_io.h"

//***
//*** global variables
//***


//***
//*** code
//***

int send_data_frame(int fd, unsigned char* wrbuffer, unsigned char count) {

   int wrlen;
   unsigned short int crc;
   unsigned char head[4];

   head[0]=0x68;
   head[1]=count;
   head[2]=count;
   head[3]=0x68;

   wrlen = write(fd, head, sizeof(head));
   if (wrlen != 4) {
       printf("Error from write: %d, %d\n", wrlen, errno);
   }
   tcdrain(fd); 

   wrlen = write(fd, wrbuffer, count);
   if (wrlen != count) {
       printf("Error from write: %d, %d\n", wrlen, errno);
   }
   tcdrain(fd); 

   crc = crc16_byte(wrbuffer, count);
   wrlen = write(fd, (unsigned char*)&crc, sizeof(crc));

   if (wrlen != 2) {
       printf("Error from write: %d, %d\n", wrlen, errno);
   }

   tcdrain(fd); 
   return 0;
}


int recv_data_frame(int fd, unsigned char* rdbuffer, unsigned char max_rdbuffer_size) {

   int rdlen;
   int head_idx;
   int buffer_idx;
   int buffer_len;
   unsigned short int crc_rcv;
   unsigned short int crc_frm;
   time_t s_time;
   // time_t e_time ;

   // hunt for frame header and consume header
   head_idx=0;

   s_time=time(NULL);
   while ((time(NULL) - s_time < SERTIMEOUT) && (head_idx < 4)) {
      rdlen = read(fd, &rdbuffer[head_idx], 1);
      if (rdlen > 0) {
         switch (head_idx) {
            case 0:
               if (rdbuffer[head_idx] == 0x68) {
                  head_idx++;
               }
               break;
            case 1:
               buffer_len = rdbuffer[head_idx];
               if(max_rdbuffer_size < buffer_len+2) {
                  // buffer to small
                  return -4;
               }
               head_idx++;
               break;
            case 2:
               if (rdbuffer[head_idx] == buffer_len) { 
                  head_idx++; 
               } else {
                  head_idx=0;
               }
               break;
            case 3:
               if (rdbuffer[head_idx] == 0x68) {
                  head_idx ++;
               } else {
                  head_idx=0;
               }
               break;
            default:
              return -2;
         }
      }
   }

   if(head_idx < 4) {
      // timeout
      return -1;
   } 
  
   // consume frame body + CRC bytes
   buffer_idx=0; 
   s_time=time(NULL);
   while ((time(NULL) - s_time < SERTIMEOUT) && (buffer_idx < buffer_len+2)) { // with crc
      rdlen = read(fd, &rdbuffer[buffer_idx], 1);
      if (rdlen > 0) {
         buffer_idx++;
      }
   }

   if (buffer_idx < (buffer_len+2)) {
      // timeout
      return -1;
   }

   crc_rcv = crc16_byte(rdbuffer, buffer_len);
   crc_frm = rdbuffer[buffer_len] + (rdbuffer[buffer_len+1] <<8);

   if (crc_rcv != crc_frm) {
       printf("Error frame crc not corect  crc_rcv:%02x crc_frmi:%02x\n", crc_rcv, crc_frm);
      return -3; 
   }
   return(buffer_len);
}


int set_ar_attr(int fd, int speed) {
    struct termios ttyopt;

    if (tcgetattr(fd, &ttyopt)) {
        perror("Error from tcgetattr()");
        return(-1);
    }

    cfsetospeed(&ttyopt, (speed_t)speed);
    cfsetispeed(&ttyopt, (speed_t)speed);
    cfmakeraw(&ttyopt);

    // like function cfmakeraw() 

    ttyopt.c_cflag &= ~(CSIZE | PARENB | CSTOPB | CRTSCTS | HUPCL | IGNBRK);
    ttyopt.c_cflag |= (CLOCAL | CREAD | CS8);
    ttyopt.c_oflag &= ~OPOST;
    ttyopt.c_iflag &= ~(BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON | OCRNL);
    ttyopt.c_iflag |= (IGNBRK);
    ttyopt.c_lflag &= ~(ECHO | ECHONL | ECHOE | ECHOK | ICANON | ISIG | IEXTEN | ECHOKE | ECHOCTL);


    /* fetch one byte */
    ttyopt.c_cc[VMIN] = 0;
    ttyopt.c_cc[VTIME] = 10;    // in tenths of a seconds

    tcflush( fd, TCIFLUSH );
    if (tcsetattr(fd, TCSANOW, &ttyopt)) {
        perror("Error from tcsetattr()");
        return(-1);
    }
    return(0);
}


int bl_hack_fast_startup(int fd, int bootloader) {
    unsigned char wrbuffer[2];
    int ret;

    switch(bootloader) {
        case BOOTLOADER_OPTIBOOT:
        default:
        // OPTIBOOT:
        // Baudrate is 115200
        // send an invalid crap byte, followed by a non 0x20 byte to cause
        // an immediate WDT reset by optiboot and start the payload
    
        // perform a shortcut of the delay introduced by the optiboot
        // bootloader (Arduino nano perfor5ms a hardware-reset when DTR
        // is asserted - which will be asserted as a result of opening
        // the serial port.
        // also read: <https://github.com/Optiboot/optiboot/wiki/HowOptibootWorks>
        // min wait: 70 ms for optiboot to be ready
        // to be sure, wait 100ms
        usleep(100000);
        wrbuffer[0]=0x90;
        wrbuffer[1]=0x22;
        ret = write(fd, wrbuffer, 2);
        tcdrain(fd); 
        // min wait 150 ms for optiboot to terminate and the payload to start up
        // give some extra time to be on the safe side
        usleep(200000);
        // now, commands are accepted
        break;
    case BOOTLOADER_ATMEGABOOT:
        // original Arduino Bootloader "ATmegaBOOT"
        // Baudrate is 57600
        // send a 'Q' character to quit bootloader quickly via WDT reset
        set_ar_attr(fd, B57600);
        // min wait 250ms required for bootloader to be ready
        usleep(350000);
        wrbuffer[0]='Q';
        ret = write(fd, wrbuffer, 1);
        tcdrain(fd); 
        set_ar_attr(fd, B115200);
        // min wait 180ms required for WDT reset and application start
        usleep(250000);
        break;
    }

    return(0);
}
